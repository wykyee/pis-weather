from rest_framework import mixins, viewsets
from .models import City


class CityViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    pass